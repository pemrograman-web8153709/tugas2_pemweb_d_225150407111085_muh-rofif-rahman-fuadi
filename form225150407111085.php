<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 2 Form</title>
</head>
<body>
    <h1>Tugas Form 225150407111085 Muh. Rofif Rahman Fuadi</h1>

    <form method="post" action="form225150407111085.php">
        Nama : <br> <input name="nama" type="text"/><br>
        NIM : <br> <input name="nim" type="text"/><br>
        Alamat : <br> <input name="alamat" type="text"/><br>
        No Telp : <br> <input name="telp" type="tel"/><br>
        Jenis Kelamin : <br> <input name="jenisKelamin" type="radio" value="Laki-Laki" id="laki-laki"/>
        <label for="laki-laki">Laki-Laki</label>
        <input name="jenisKelamin" type="radio" value="Perempuan" id="perempuan"/>
        <label for="perempuan">Perempuan</label><br>
        <label for="tanggalLahir">Tanggal Lahir : </label> <br>
        <input type="date" id="tanggalLahir" name="tanggalLahir"><br>
        <label for="minat">Program Studi :</label>
        <br>
        <select id="programStudi" name="programStudi">
        <option value="Sistem Informasi">Sistem Informasi</option>
        <option value="Teknologi Informasi">Teknologi Informasi</option>
        <option value="Pendidikan Teknologi Informasi">Pendidikan Teknologi Informasi</option>
        <option value="Teknik Informatika">Teknik Informatika</option>
        <option value="Teknik Komputer">Teknik Komputer</option>
        </select> <br>
        <br><input type="submit"/>
    </form>

    <br>
    
    <?php
    if (isset($_POST["nim"])){?>
        <table border="1">
            <tr>
                <th>
                    Nama
                </th>
                <th>
                    NIM
                </th>
                <th>
                    Alamat
                </th>
                <th>
                    No Telp
                </th>
                <th>
                    Jenis Kelamin
                </th>
                <th>
                    Tanggal Lahir
                </th>
                <th>
                    Program Studi
                </th>
            </tr>
            <tr>
                <td>
                    <?php
                    echo $_POST["nama"];
                    ?>
                </td>
                <td>
                    <?php
                    echo $_POST["nim"];
                    ?>
                </td>
                <td>
                    <?php
                    echo $_POST["alamat"];
                    ?>
                </td>
                <td>
                    <?php
                    echo $_POST["telp"];
                    ?>
                </td>
                <td>
                    <?php
                    echo $_POST["jenisKelamin"];
                    ?>
                </td>
                <td>
                    <?php
                    echo $_POST["tanggalLahir"];
                    ?>
                </td>
                <td>
                    <?php
                    echo $_POST["programStudi"];
                    ?>
                </td>
            </tr>
        </table>
    <?php
    }
    ?>
    
</body>
</html>